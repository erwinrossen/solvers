import os.path
from unittest import TestCase, skip

import json
import math

from sat_utils import Q, neg, solve_all

SRC_DIR = os.path.dirname(__file__)
ROOT_DIR = os.path.dirname(SRC_DIR)
DATA_DIR = os.path.join(ROOT_DIR, 'data', 'test')

with open(os.path.join(DATA_DIR, 'at_most_4_out_of_9.json'), 'r') as f:
    at_most_4_out_of_9 = [
        tuple(clause) if clause else ()
        for clause in json.load(f)
    ]
puzzle_and_k_to_all_solutions = {
    # 2 facts
    (frozenset(('A', 'B')), 0): [('~A', '~B')],
    (frozenset(('A', 'B')), 1): [('A', '~B'), ('B', '~A'), ('~A', '~B')],
    (frozenset(('A', 'B')), 2): [()],

    # 3 facts
    (frozenset(('A', 'B', 'C')), 0):
        [('~A', '~B', '~C')],
    (frozenset(('A', 'B', 'C')), 1):
        [('A', '~B', '~C'), ('B', '~A', '~C'), ('C', '~A', '~B'), ('~A', '~B', '~C')],
    (frozenset(('A', 'B', 'C')), 2):
        [('A', 'B', '~C'), ('A', 'C', '~B'), ('A', '~B', '~C'), ('B', 'C', '~A'), ('B', '~A', '~C'), ('C', '~A', '~B'),
         ('~A', '~B', '~C')],
    (frozenset(('A', 'B', 'C')), 3):
        [()],

    # 4 facts
    (frozenset(('A', 'B', 'C', 'D')), 0):
        [('~A', '~B', '~C', '~D')],
    (frozenset(('A', 'B', 'C', 'D')), 1):
        [('A', '~B', '~C', '~D'), ('B', '~A', '~C', '~D'), ('C', '~A', '~B', '~D'), ('D', '~A', '~B', '~C'),
         ('~A', '~B', '~C', '~D')],
    (frozenset(('A', 'B', 'C', 'D')), 2):
        [('A', 'B', '~C', '~D'), ('A', 'C', '~B', '~D'), ('A', 'D', '~B', '~C'), ('A', '~B', '~C', '~D'),
         ('B', 'C', '~A', '~D'), ('B', 'D', '~A', '~C'), ('B', '~A', '~C', '~D'), ('C', 'D', '~A', '~B'),
         ('C', '~A', '~B', '~D'), ('D', '~A', '~B', '~C'), ('~A', '~B', '~C', '~D')],
    (frozenset(('A', 'B', 'C', 'D')), 3):
        [('A', 'B', 'C', '~D'), ('A', 'B', 'D', '~C'), ('A', 'B', '~C', '~D'), ('A', 'C', 'D', '~B'),
         ('A', 'C', '~B', '~D'), ('A', 'D', '~B', '~C'), ('A', '~B', '~C', '~D'), ('B', 'C', 'D', '~A'),
         ('B', 'C', '~A', '~D'), ('B', 'D', '~A', '~C'), ('B', '~A', '~C', '~D'), ('C', 'D', '~A', '~B'),
         ('C', '~A', '~B', '~D'), ('D', '~A', '~B', '~C'), ('~A', '~B', '~C', '~D')],
    (frozenset(('A', 'B', 'C', 'D')), 4): [()],

    # 9 facts
    (frozenset(('A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J')), 1):
        [('A', '~B', '~C', '~D', '~E', '~F', '~G', '~H', '~I', '~J'),
         ('B', '~A', '~C', '~D', '~E', '~F', '~G', '~H', '~I', '~J'),
         ('C', '~A', '~B', '~D', '~E', '~F', '~G', '~H', '~I', '~J'),
         ('D', '~A', '~B', '~C', '~E', '~F', '~G', '~H', '~I', '~J'),
         ('E', '~A', '~B', '~C', '~D', '~F', '~G', '~H', '~I', '~J'),
         ('F', '~A', '~B', '~C', '~D', '~E', '~G', '~H', '~I', '~J'),
         ('G', '~A', '~B', '~C', '~D', '~E', '~F', '~H', '~I', '~J'),
         ('H', '~A', '~B', '~C', '~D', '~E', '~F', '~G', '~I', '~J'),
         ('I', '~A', '~B', '~C', '~D', '~E', '~F', '~G', '~H', '~J'),
         ('J', '~A', '~B', '~C', '~D', '~E', '~F', '~G', '~H', '~I'),
         ('~A', '~B', '~C', '~D', '~E', '~F', '~G', '~H', '~I', '~J')],
    (frozenset(('A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J')), 4): at_most_4_out_of_9,
}


class SatUtilsTestCase(TestCase):
    # Binomial

    def test_at_most_k_binomial__verify_cnf(self):
        with self.subTest('2 facts'):
            q = Q('A', 'B')
            self.assertEqual(q.at_most_k_binomial(0), [('~A',), ('~B',)])
            self.assertEqual(q.at_most_k_binomial(1), [('~A', '~B')])
            self.assertEqual(q.at_most_k_binomial(2), [])

        with self.subTest('3 facts'):
            q = Q('A', 'B', 'C')
            self.assertEqual(q.at_most_k_binomial(0), [('~A',), ('~B',), ('~C',)])
            self.assertEqual(q.at_most_k_binomial(1), [('~A', '~B'), ('~A', '~C'), ('~B', '~C')])
            self.assertEqual(q.at_most_k_binomial(2), [('~A', '~B', '~C')])
            self.assertEqual(q.at_most_k_binomial(3), [])

        with self.subTest('4 facts'):
            q = Q('A', 'B', 'C', 'D')
            self.assertEqual(q.at_most_k_binomial(0), [('~A',), ('~B',), ('~C',), ('~D',)])
            self.assertEqual(q.at_most_k_binomial(1), [('~A', '~B'), ('~A', '~C'), ('~A', '~D'),
                                                       ('~B', '~C'), ('~B', '~D'), ('~C', '~D')])
            self.assertEqual(q.at_most_k_binomial(2), [('~A', '~B', '~C'), ('~A', '~B', '~D'),
                                                       ('~A', '~C', '~D'), ('~B', '~C', '~D')])
            self.assertEqual(q.at_most_k_binomial(3), [('~A', '~B', '~C', '~D')])
            self.assertEqual(q.at_most_k_binomial(4), [])

    def test_at_most_k_binomial__verify_properties(self):
        """
        Verify properties of this encoding, stated in the PDF
        """

        facts = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J']
        n = len(facts)
        q = Q(*facts)
        for k in range(10):
            cnf = q.at_most_k_binomial(k)

            # This encoding introduces no new variables
            extra_variables = {
                fact.replace('~', '') for clause in cnf for fact in clause
                if (fact not in facts and neg(fact) not in facts)
            }
            self.assertEqual(len(extra_variables), 0)

            # This encoding comprises ( n k+1 ) clauses
            expected_nr_clauses = math.comb(n, k + 1)
            self.assertEqual(len(cnf), expected_nr_clauses, f'{k=}, {cnf=}')

            # each of size k + 1
            for clause in cnf:
                self.assertEqual(len(clause), k + 1)

    def test_at_most_k_binomial__solutions(self):
        for (puzzle, k), solutions in puzzle_and_k_to_all_solutions.items():
            with self.subTest(f'{puzzle=}, {k=}'):
                q = Q(*puzzle)
                cnf = q.at_most_k_binomial(k)
                all_solutions = solve_all(cnf, include_neg=True)
                self.assertEqual(all_solutions, solutions, f'{all_solutions=}')

    # Binary (k=1)

    def test_at_most_one_binary__verify_properties(self):
        """
        Verify properties of this encoding, stated in the PDF
        """

        facts = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J']
        n = len(facts)
        q = Q(*facts)
        for k in range(10):
            cnf = q.at_most_one_binary()
            log2n = math.ceil(math.log2(n))

            # This encoding introduces |log2 n| new variables
            extra_variables = {
                fact.replace('~', '') for clause in cnf for fact in clause
                if (fact not in facts and neg(fact) not in facts)
            }
            self.assertEqual(len(extra_variables), log2n)

            # This encoding comprises n |log2 n| clauses
            expected_nr_clauses = n * log2n
            self.assertEqual(len(cnf), expected_nr_clauses, f'{k=}, {cnf=}')

    def test_at_most_one_binary__solutions(self):
        for (puzzle, k), solutions in puzzle_and_k_to_all_solutions.items():
            if k != 1:
                # This method can only handle k=1
                continue
            with self.subTest(f'{puzzle=}, {k=}'):
                q = Q(*puzzle)
                cnf = q.at_most_one_binary()
                all_solutions = solve_all(cnf, include_neg=True)
                self.assertEqual(all_solutions, solutions, f'{all_solutions=}')

    # Binary (generic k)

    @skip('Not implemented')
    def test_at_most_k_binary__solutions(self):
        for (puzzle, k), solutions in puzzle_and_k_to_all_solutions.items():
            with self.subTest(f'{puzzle=}, {k=}'):
                print(f'{puzzle=}, {k=}, expected solution={solutions}')
                q = Q(*sorted(puzzle))
                cnf = q.at_most_k_binary(k)
                all_solutions = solve_all(cnf, include_neg=True)
                self.assertEqual(all_solutions, solutions, f'{all_solutions=}')
