import collections

from typing import Collection

import time
from sys import intern

import itertools
import string
from enum import Enum

from sat_utils import basic_fact, from_dnf, one_of, solve_one, is_ext_var, Fact, ext_var, neg, Q

Point = str


class Level(Enum):
    easy = 0
    hard = 1
    extreme = 2
    unreasonable = 3


def comb(point: Point, value: int) -> Fact:
    """
    Format a fact (a value assigned to a given point), and store it into the interned strings table

    :param point: Point on the grid, characterized by two letters, e.g. AB
    :param value: Value of the cell on that point, e.g. 2
    :return: Fact string 'AB 2'
    """

    return intern(f'{point} {value}')


def visible_from_line(line: list[int], reverse: bool = False) -> int:
    """
    Return how many towers are visible from the given line

    >>> visible_from_line([1, 2, 3, 4])
    4
    >>> visible_from_line([1, 4, 3, 2])
    2
    """

    visible = 0
    highest_seen = 0
    for number in reversed(line) if reverse else line:
        if number > highest_seen:
            visible += 1
            highest_seen = number
    return visible


def visible_in_span(points: Collection[str], desired: int) -> "cnf":
    """Assert desired visible towers in span. Wlog, visibility is from index 0."""
    points = list(points)
    n = len(points)
    assert desired <= n

    cnf = []

    is_kth_visible_tower_at = {}
    is_kth_visible_tower_vars = collections.defaultdict(list)
    is_visible_tower_at = {}
    for point in points:
        is_visible_tower_vars = []
        for k in range(1, n + 1):
            # Xvk
            is_kth_visible_tower_var = ext_var()

            is_kth_visible_tower_at[(point, k)] = is_kth_visible_tower_var
            is_kth_visible_tower_vars[k].append(is_kth_visible_tower_var)
            is_visible_tower_vars.append(is_kth_visible_tower_var)

        # Xv
        is_visible_tower_at_var = ext_var()
        # Xv → (Xv1 ∨ Xv2 ∨ ⋯)
        cnf.append(tuple([neg(is_visible_tower_at_var)] + is_visible_tower_vars))
        # (Xv1 ∨ Xv2 ∨ ⋯) → Xv
        for is_visible_tower_var in is_visible_tower_vars:
            cnf.append((neg(is_visible_tower_var), is_visible_tower_at_var))

        is_visible_tower_at[point] = is_visible_tower_at_var

        # At most one visible tower here.
        cnf += Q(*is_visible_tower_vars) <= 1

    # At most one kth visible tower anywhere.
    for k in range(1, n + 1):
        cnf += Q(*is_kth_visible_tower_vars[k]) <= 1

    # Towers are ordered.
    for index, point in enumerate(points):
        if index == 0:
            cnf += basic_fact(is_kth_visible_tower_at[(point, 1)])
            continue

        for k in range(1, n + 1):
            # Xvk → ⋯
            implication = [neg(is_kth_visible_tower_at[(point, k)])]

            j = k - 1
            if j > 0:
                for index_j, point_j in enumerate(points):
                    if index_j == index:
                        break

                    # ⋯ ∨ Wxj ∨ ⋯
                    implication.append(is_kth_visible_tower_at[(point_j, j)])

            cnf.append(tuple(implication))

    is_height_obscured_at = {}
    is_height_obscured_previous = [None] * n
    for point in points:
        is_obscured_previous = None
        for k in range(1, n + 1):
            # Xok
            is_height_obscured_var = ext_var()

            # Wok → Xok
            is_k_obscured_previous = is_height_obscured_previous[k - 1]
            if is_k_obscured_previous is not None:
                cnf.append((neg(is_k_obscured_previous), is_height_obscured_var))

            # Xok → Xo(k-1)
            if is_obscured_previous is not None:
                cnf.append((neg(is_height_obscured_var), is_obscured_previous))

            is_height_obscured_at[(point, k)] = is_height_obscured_var
            is_height_obscured_previous[k - 1] = is_height_obscured_var
            is_obscured_previous = is_height_obscured_var

    # A placed tower obscures smaller later towers.
    for index, point in enumerate(points):
        if index + 1 == len(points):
            break

        next_point = points[index + 1]
        for k in range(2, n + 1):
            j = k - 1

            # Xk → Yo(k-1)
            cnf.append((neg(comb(point, k)), is_height_obscured_at[(next_point, j)]))

    is_height_visible_at = {}
    for point in points:
        for k in range(1, n + 1):
            # Xhv
            height_visible_at_var = ext_var()

            # Xhv ≡ (Xh ∧ Xv)
            cnf.append((neg(height_visible_at_var), comb(point, k)))
            cnf.append((neg(height_visible_at_var), is_visible_tower_at[point]))
            cnf.append(
                (
                    neg(comb(point, k)),
                    neg(is_visible_tower_at[point]),
                    height_visible_at_var,
                )
            )

            is_height_visible_at[(point, k)] = height_visible_at_var

    for point in points:
        for k in range(1, n + 1):
            # Xok → ¬Xkv
            cnf.append(
                (
                    neg(is_height_obscured_at[(point, k)]),
                    neg(is_height_visible_at[(point, k)]),
                )
            )

    # At least one of the towers is the desired kth visible.
    cnf.append(tuple(is_kth_visible_tower_vars[desired]))

    # None of the towers can be visible above the desired kth.
    if desired < n:
        for is_kth_visible_tower_var in is_kth_visible_tower_vars[desired + 1]:
            cnf += basic_fact(neg(is_kth_visible_tower_var))

    return cnf


class TowersPuzzle:
    def __init__(self, grid_size: int = 4, level: Level = Level.easy):
        self._set_puzzle(grid_size, level)
        self._cnf = None
        self._solution = None

    def _set_puzzle(self, grid_size: int, level: Level):
        if grid_size == 4:
            if level == Level.easy:
                self.visible_from_top = [3, 3, 2, 1]
                self.visible_from_bottom = [1, 2, 3, 2]
                self.visible_from_left = [3, 3, 2, 1]
                self.visible_from_right = [1, 2, 3, 2]
                self.given_numbers = {'AC': 3}
        elif grid_size == 5:
            if level == Level.easy:
                self.visible_from_top = [3, 2, 1, 4, 2]
                self.visible_from_bottom = [2, 2, 4, 1, 2]
                self.visible_from_left = [3, 2, 3, 1, 3]
                self.visible_from_right = [2, 2, 1, 3, 2]
                self.given_numbers = {}
            elif level == Level.hard:
                self.visible_from_top = [None, None, None, 3, None]
                self.visible_from_bottom = [3, 3, None, None, 2]
                self.visible_from_left = [None, 3, None, None, None]
                self.visible_from_right = [4, None, 2, None, None]
                self.given_numbers = {'CA': 3}
        elif grid_size == 9:
            if level == Level.easy:
                self.visible_from_top = [3, 3, 3, 3, 1, 4, 2, 4, 2]
                self.visible_from_bottom = [3, 1, 4, 2, 5, 3, 3, 2, 3]
                self.visible_from_left = [3, 3, 1, 2, 4, 5, 2, 3, 2]
                self.visible_from_right = [3, 1, 7, 4, 3, 3, 2, 2, 4]
                self.given_numbers = {'AB': 5, 'AD': 4, 'BD': 3, 'BE': 2, 'CD': 7, 'CF': 5, 'CG': 1, 'DB': 1, 'DH': 7,
                                      'EA': 4, 'EI': 2, 'FA': 2, 'FE': 8, 'GG': 7, 'GI': 6, 'HA': 3, 'HF': 2, 'HH': 1,
                                      'IG': 6}
            elif level == Level.unreasonable:
                self.visible_from_top = [2, 3, None, 2, None, 2, None, None, 4]
                self.visible_from_bottom = [4, None, None, 4, None, 1, 5, None, 2]
                self.visible_from_left = [3, 4, None, 2, 5, None, 3, 2, 3]
                self.visible_from_right = [2, 3, 3, 4, None, None, None, None, None]
                self.given_numbers = {'AG': 3, 'BC': 3, 'CB': 5, 'CE': 2, 'EA': 3,
                                      'EB': 2, 'FC': 6, 'GD': 1, 'HD': 4, 'IH': 1}

        if not hasattr(self, 'visible_from_top'):
            msg = f'No puzzle of grid size {grid_size} and level {level} is defined'
            raise ValueError(msg)

    def display_puzzle(self):
        print('*** Puzzle ***')
        self._display(self.given_numbers)

    def display_solution(self):
        print('*** Solution ***')
        # point_to_value = {point: value for point, value in [fact.split() for fact in self.solution]}
        point_to_value = {
            point: value
            for point, value in [
                fact.split() for fact in self.solution if not is_ext_var(fact)
            ]
        }
        self._display(point_to_value)

    @property
    def n(self) -> int:
        """
        :return: Size of the grid
        """

        return len(self.visible_from_top)

    @property
    def points(self) -> list[Point]:
        return [''.join(letters) for letters in itertools.product(string.ascii_uppercase[:self.n], repeat=2)]

    @property
    def rows(self) -> list[list[Point]]:
        """
        :return: Points, grouped per row
        """

        return [self.points[i:i + self.n] for i in range(0, self.n * self.n, self.n)]

    @property
    def cols(self) -> list[list[Point]]:
        """
        :return: Points, grouped per column
        """

        return [self.points[i::self.n] for i in range(self.n)]

    @property
    def values(self) -> list[int]:
        return list(range(1, self.n + 1))

    @property
    def cnf(self):
        if self._cnf is None:
            cnf = []

            # Each point assigned exactly one value
            for point in self.points:
                cnf += one_of(*(comb(point, value) for value in self.values))

            # Each value gets assigned to exactly one point in each row
            for row in self.rows:
                for value in self.values:
                    cnf += one_of(*(comb(point, value) for point in row))

            # Each value gets assigned to exactly one point in each col
            for col in self.cols:
                for value in self.values:
                    cnf += one_of(*(comb(point, value) for point in col))

            implementation = 'old'
            if implementation == 'old':
                # Set visible from left
                if self.visible_from_left:
                    for index, row in enumerate(self.rows):
                        target_visible = self.visible_from_left[index]
                        if not target_visible:
                            continue
                        possible_perms = []
                        for perm in itertools.permutations(range(1, self.n + 1)):
                            if visible_from_line(perm) == target_visible:
                                possible_perms.append(tuple(
                                    comb(point, value)
                                    for point, value in zip(row, perm)
                                ))
                        cnf += from_dnf(possible_perms)

                # Set visible from right
                if self.visible_from_right:
                    for index, row in enumerate(self.rows):
                        target_visible = self.visible_from_right[index]
                        if not target_visible:
                            continue
                        possible_perms = []
                        for perm in itertools.permutations(range(1, self.n + 1)):
                            if visible_from_line(perm, reverse=True) == target_visible:
                                possible_perms.append(tuple(
                                    comb(point, value)
                                    for point, value in zip(row, perm)
                                ))
                        cnf += from_dnf(possible_perms)

                # Set visible from top
                if self.visible_from_top:
                    for index, col in enumerate(self.cols):
                        target_visible = self.visible_from_top[index]
                        if not target_visible:
                            continue
                        possible_perms = []
                        for perm in itertools.permutations(range(1, self.n + 1)):
                            if visible_from_line(perm) == target_visible:
                                possible_perms.append(tuple(
                                    comb(point, value)
                                    for point, value in zip(col, perm)
                                ))
                        cnf += from_dnf(possible_perms)

                # Set visible from bottom
                if self.visible_from_bottom:
                    for index, col in enumerate(self.cols):
                        target_visible = self.visible_from_bottom[index]
                        if not target_visible:
                            continue
                        possible_perms = []
                        for perm in itertools.permutations(range(1, self.n + 1)):
                            if visible_from_line(perm, reverse=True) == target_visible:
                                possible_perms.append(tuple(
                                    comb(point, value)
                                    for point, value in zip(col, perm)
                                ))
                        cnf += from_dnf(possible_perms)
            elif implementation == 'new':
                # Follow the answer provided here: https://stackoverflow.com/a/71542523/1469465
                # Note: this implementation is incorrect as it is now. I need to spend more time to look
                # into the details of how to encode this, and how to fix it. I leave it in here as a
                # reference for future work.
                # Set visible from left
                if self.visible_from_left:
                    for index, row in enumerate(self.rows):
                        target_visible = self.visible_from_left[index]
                        if not target_visible:
                            continue

                        cnf += visible_in_span(row, target_visible)

                # Set visible from right
                if self.visible_from_right:
                    for index, row in enumerate(self.rows):
                        target_visible = self.visible_from_right[index]
                        if not target_visible:
                            continue

                        cnf += visible_in_span(reversed(row), target_visible)

                # Set visible from top
                if self.visible_from_top:
                    for index, col in enumerate(self.cols):
                        target_visible = self.visible_from_top[index]
                        if not target_visible:
                            continue

                        cnf += visible_in_span(col, target_visible)

                # Set visible from bottom
                if self.visible_from_bottom:
                    for index, col in enumerate(self.cols):
                        target_visible = self.visible_from_bottom[index]
                        if not target_visible:
                            continue

                        cnf += visible_in_span(reversed(col), target_visible)


            # Set given numbers
            for point, value in self.given_numbers.items():
                cnf += basic_fact(comb(point, value))

            self._cnf = cnf

        return self._cnf

    @property
    def solution(self):
        if self._solution is None:
            start_time = time.perf_counter()
            cnf = self.cnf
            cnf_time = time.perf_counter()
            print(f'CNF: {cnf_time - start_time}s')

            self._solution = solve_one(cnf)
            end_time = time.perf_counter()
            print(f'Solve: {end_time - cnf_time}s')
        return self._solution

    def _display(self, facts: dict[Point, int]):
        top_line = '    ' + ' '.join([str(elem) if elem else ' ' for elem in self.visible_from_top]) + '    '
        print(top_line)
        print('-' * len(top_line))
        for index, row in enumerate(self.rows):
            elems = [str(self.visible_from_left[index] or ' '), '|'] + \
                    [str(facts.get(point, ' ')) for point in row] + \
                    ['|', str(self.visible_from_right[index] or ' ')]
            print(' '.join(elems))
        print('-' * len(top_line))
        bottom_line = '    ' + ' '.join([str(elem) if elem else ' ' for elem in self.visible_from_bottom]) + '    '
        print(bottom_line)
        print()


if __name__ == '__main__':
    puzzle = TowersPuzzle(grid_size=5, level=Level.hard)
    # puzzle = TowersPuzzle(grid_size=9, level=Level.unreasonable)
    puzzle.display_puzzle()
    puzzle.display_solution()
